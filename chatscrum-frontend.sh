#!/bin/bash
cd /chatscrum_backend/chatscrum-frontend
sudo docker build -t chatscrumfrontend .
sudo docker run --env-file=.env chatscrumfrontend
echo "successfully built and deployed the frontend image"
