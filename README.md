# Chatscrum Frontend

# Deploying the Chatscrum on an S3 Bucket
This guide explains how to deploy the Chatscrum application on AWS S3 using Docker. Chatscrum is made up of two microservices, the frontend, and the backend. The backend microservice is located in the repository https://gitlab.com/showpopulous/scrumastr, which should be deployed before building and deploying the frontend.

To get started with deploying the Chatscrum frontend, follow these steps:

## 1. Getting AWS Credentials

Create an AWS account and create an IAM user with programmatic access. This user should have the `AmazonS3FullAccess` permission.

## 2. Creating an S3 bucket

Create an S3 bucket with the same name as the subdomain.doman.tld for your Chatscrum site. 

For example, the bucket name for "int" would be "int.chatscrum.com". 

Use the `us-west-2` region.

## 3. Allowing public access on S3

Allow public access on the S3 bucket by going to the Permissions tab and unchecking `"Block public access through new public bucket"` and `"Block public and cross-account access through new public bucket"`.

## 4 Enable Static site hosting on your S3 Bucket
Enable static site hosting on your S3 bucket by clicking on the Properties tab, then the "Static website hosting" option, and selecting "Use this bucket to host a website". Define the index and error document as `index.html`.

## 5. Set GetObject policy to S3 bucket.

Set the GetObject policy for the S3 bucket by applying a policy that will grant anonymous users access to the data. Copy the policy code provided in the guide, replacing "your-BucketName" with your actual bucket name
```
{
    "Version": "2012-10-17",
    "Id": "Policy1631774969848",
    "Statement": [
        {
            "Sid": "Stmt1631774967681",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::your-BucketName/*"
        }
    ]
} 
```

## 6. Clone the Repository

You need to install git and [Docker](https://docs.docker.com/engine/install/centos/) on your machine then run the clone command

`sudo git clone <repository url>`

For example, `sudo git clone https://gitlab.com/showpopulous/chatscrum-frontend.git`

## 7. Navigate into the chatscrum-frontend folder
`cd chatscrum-frontend`

## 8. Set enviornment variables

Modify a file named `.env`

`sudo vi .env`

Inside this file set the following variables. They should all belong to the user you created in step 1.
```
AWS_ACCESS_KEY_ID=<id>
AWS_SECRET_ACCESS_KEY=<key>
AWS_DEFAULT_REGION=<region>
```
Save the file. 

## 9. Set bucket name
Set the bucket name created in step 2 by navigating to the Dockerfile and modifying the following command: 

```CMD ["s3", "sync", "./", "s3://<buckt-name>"]```

## 10. Set the API_URL

Open the second enviornment file and set the following variables. 

`sudo vi chatscrum_react/.env`

at this point you should already have you Chatscrum Backend app running and also a websocket url setup. Set your api url.

```
REACT_APP_DOMAIN_NAME: 'backend-endpoint',
```

For example

```
REACT_APP_DOMAIN_NAME: '3.142.120.241:80/django',
```
NB: To set up domain_name reference https://gitlab.com/showpopulous/scrumastr#building-chatscrum. 

## 11. Build the Docker Image

Run the command `sudo docker build -t chatscrum .` 

## 12. Run the Built Image

Note I have define the  .env file when running the image

`sudo docker run --env-file=.env chatscrum`

You should see the upload be done. If your AWS credentials are wrong it will give an error.
