import React from 'react';
// import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
// import { faCircle } from '@fortawesome/free-solid-svg-icons';
import './scrumboard.css';
// import { Link } from 'react-router-dom';
// import { useNavigate } from 'react-router-dom';
import { useState, useEffect, useRef } from 'react';
import './styles.scss';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';

function Scrumboard() {
const domainname = process.env.REACT_APP_DOMAIN_NAME;

const [TFW,setTFW] = useState([])
const [participants,setParticipants] = useState([])
const [TFTW,setTFTW] = useState([])
const [tftwList,setTftwList] = useState([])
const [tftdList,setTftdList] = useState([])
const [loggedUserId,setLoggedUserId] = ("")
const [loggedUser,setLoggedUser] = useState(sessionStorage.getItem('realname'))
const [loggedUserProfile,setLoggedUserProfile] = useState(sessionStorage.getItem('realname'))
const [loggedProject,setLoggedProject] = useState(sessionStorage.getItem('project_name'))
const [projectID,setprojectID] = useState(sessionStorage.getItem('project_id'))
const [username,setUsername] = useState(sessionStorage.getItem('realname'))
const [listUsers,setListUsers] = useState([])
const [sprints, setSprints] = useState([])
const [users, setUsers] = useState([])
const [verify, setVerify] = useState([])
const [done, setDone] = useState([])
const [addToUser,setAddToUser] = useState(sessionStorage.getItem('role_id'))
const [activeTab, setActiveTab] = useState('profile');
const navigate = useNavigate();

const [goalName,setGoalName] = useState("")
const [pushID,setpushID] = useState("")
const [hours,setHours] = useState("")
const [taskToEdit,setTaskToEdit] = useState("")
const [loggedUserRole,setLoggedUserRole] = useState(sessionStorage.getItem('role'))
const [currentSprint,setCurrentSprint] = useState([])
const [loggedSprint, setLoggedSprint] = useState({
    sprintID: '',
    dateCreated: '',
    endDate: ''
  });
const [usesSlack,setUsesSlack] = useState(sessionStorage.getItem('user_slack'))
const [changeLoggedSprint,setChangeLoggedSprint] = useState(false)
const [verifyList,setVerifyList] = useState(false)
const [doneList,setDoneList] = useState(false)

const logoutModalref = useRef(null)
const hidesref = useRef(null)
const modaref = useRef(null)
const openEditTaskModalref = useRef(null)
const projectsDDContentref= useRef(null)
const sprintAlertref = useRef(null)
const sprintDDContentref = useRef(null)
const Alertref = useRef(null)
const addTaskBtnref = useRef(null)
const addNoteBtnref = useRef(null)
const dialogref = useRef(null)
const [addTaskAndNoteBtnVisible, setAddTaskAndNoteBtnVisible] = useState(true);

const handleTabClick = (tabID) =>{
  setActiveTab(tabID);
}

function autoHideDialog(){
  dialogref.current.style.display = 'none'
}

function openAddTaskModal(){
  modaref.current.style.display = "inline"
}

function handleDragStart(item){

}

function showAddTaskandNoteBTN(){
  addTaskBtnref.current.style.display = "inline"
  addNoteBtnref.current.style.display = "inline"
  setAddTaskAndNoteBtnVisible(true);
}
function hideAddTaskandNoteBTN(){
  addTaskBtnref.current.style.display = "none"
  addNoteBtnref.current.style.display = "none"
  setAddTaskAndNoteBtnVisible(false);
}
function showProjectTabContents(){
  projectsDDContentref.current.classList.add('ppDD')
}
function autoClearTft(){}
function createNewProject(){}
function showSprintTabContents(){
  sprintDDContentref.current.classList.add("spDD");
}
function startNewSprint(){
  if (loggedUserRole == 'Owner' || loggedUserRole == 'Admin') {
    if (loggedSprint.sprintID != ' ') {
      if (
        Date.parse(loggedSprint.endDate) > new Date().valueOf()
      ) {
        if (
          window.confirm(
            `Are You Sure You Want To End Sprint #${loggedSprint.sprintID} And Start A New Sprint?`
          )
        ) {
          startSprint();
        }
      } else {
        startSprint();
      }
    } else {
      startSprint();
    }
  } else {
    NotificationBox('Only Admin and Owner Can Create Sprint');
  }
}

function startSprint(){
  startSprintRequest(projectID).then((data) =>
  {
    NotificationBox(data['message'])
    setSprints([])
    filterSprints(data['data'])
  }
  )
  .catch((error) => {
    if (error['status'] == 401) {
      NotificationBox(
        'Session Invalid or Expired. Please Login!'
      );
      logout();
    } else {
      NotificationBox('Unexpected Error!');
    }
  })
}

function startSprintRequest(project_id){
  let sprint_start_date = new Date(new Date().getTime());
		let sprint_end_date = new Date(
			new Date().getTime() + 7 * 24 * 60 * 60 * 1000
		);
    return axios.post(domainname.concat("api/scrumsprint/?goal_project_id=".concat(projectID)),
    { project_id : project_id,
    created_on : sprint_start_date,
    ends_on : sprint_end_date},
    {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'JWT ' + sessionStorage.getItem('token'),
    },
  }
  );
  
}

function NotificationBox(alert){
Alertref.current.innerHTML = alert;
Alertref.current.className = 'show';
setTimeout(function () {
  Alertref.current.className = Alertref.current.className.replace('show', '');
}, 3000);
}

function connectSlack(){}
function appInfo(){}
function logoutModal(){
  logoutModalref.current.style.display = 'block';
}
function hideslackchat(){}
function close(){
  logoutModalref.current.style.display = 'none';
  hidesref.current.style.display = 'scroll';
  modaref.current.style.display = 'none';
  openEditTaskModalref.current.style.display = 'none';
}
function submitAddTask(event){
  event.preventDefault();
  setGoalName(event.target.elements.goal_name.value)
  if (goalName != '' && goalName != undefined) {
    if (goalName.length >= 4) {
      addTask();
    } else {
      NotificationBox('Please, describe the goal in details');
    }
  } else {
    NotificationBox('Goal name cannot be empty!');
  }
}

function addTask(){
  if (
    loggedUserRole == 'Owner' ||
    loggedUserRole == 'Admin' ||
    loggedUserId == addToUser
  ) {
    addTaskRequest(projectID, addToUser)
      .then(
        (data) => {
          NotificationBox(data['message']);
        },
        (error) => {
          if (error['status'] == 401) {
            NotificationBox(
              'Session Invalid or Expired. Please Login!'
            );
            logout();
          } else {
            NotificationBox('Add Task Failed!');
            close();
          }
        }
      );
  } else {
    this.close();
    this.NotificationBox(
      `You Can Only Add Task For ${this.loggedUser}`
    );
  }
}

function addTaskRequest(project_id,user_role_id){
  return axios.post(domainname.concat("api/scrumgoals/"),
  { name : goalName,
  user : 'm' + user_role_id,
  project_id : project_id},
  {
  headers: {
    'Content-Type': 'application/json',
    Authorization: 'JWT ' + sessionStorage.getItem('token'),
  },
}
);
}

function closeSlackModal(){}
function connectSlack(){}
function editTask(){}
function logout(){
  sessionStorage.removeItem('token');
  navigate('/home')
}
function processMoveGoalRequest(){}
function closeDialog(){}
function sprintAlertHidden(){
  if(sprintAlertref.current.classList.contains('sprintAlertVissible')){
    sprintAlertref.current.classList.replace('sprintAlertVissible', 'sprintAlertHidden')
  }
  setTimeout(() => {
    sprintAlertref.current.style.display = 'none';
  }, 300);
}

async function getAllUsersGoals(){
setprojectID(JSON.parse(sessionStorage.getItem('project_id')));

const data = await allProjectGoals(projectID);
  setParticipants(data['data']);
  console.log("participants")
  console.log(participants)

  if(participants.length != 0){
    filterUsers(participants)
  }
}

async function allProjectGoals(project_id){
const response = await fetch(domainname.concat("api/scrumprojects/"), {
  method: "GET",
  headers: {
    'Content-Type': 'application/json',
  },
});

const data = await response.json()

return data;
}

function getAllUsernames(){
allProjectUsers(projectID).then(data =>{
  setListUsers(data['data'])
})
}

async function allProjectUsers(project_id){
  return axios.post(domainname.concat("api/get_all_usernames/"),
    { project_id : project_id},
    {
    headers: {
      'Content-Type': 'application/json',
    },
  }
  );
}

function getAllSprints(){
  allSprints(projectID).then(data => {
    setSprints(data)
  },
  (error)=>{
    console.log('error',error)
  });
}

useEffect(() => {
  console.log("Sprints")
  console.log(sprints)
  if(sprints.length > 0){
   filterSprints(sprints)
   sprintAlertHidden();   
  }
  else{
    if(loggedSprint.sprintID == ' '){
      if(loggedUserRole == "Owner" || loggedUserRole == "Admin"){
        sprintAlertref.current.classList.add('sprintAlertVissible')
      }
      else{
        sprintAlertHidden();
      }
    }
  }
}, [sprints]);


function filterSprints(sprintFilter){
  sprintFilter.forEach((element) => {
    currentSprint.unshift({
      sprintID: element['id'],
      dateCreated: element['created_on'],
      endDate: element['ends_on'],
    });
  });
  if (currentSprint.length > 0) {
    setLoggedSprint(currentSprint[0]);
  }
}

async function allSprints(project_id){
  const response = await axios.get(domainname.concat('api/scrumsprint/?goal_project_id=').concat(project_id), {
    headers: {
      'Content-Type': 'application/json',
    },
  })
  return response.data;
}

const drop = (event) => {
}

 //Initialization of the page

 useEffect(() => {
  getAllUsernames();
  getAllUsersGoals();
  getAllSprints();
  hideAddTaskandNoteBTN();
  autoHideDialog();
}, [])

function filterUsers(userFilter){
const temp_users = []
const temp_TFTW = []
const temp_verify = []
const temp_done = []

userFilter.forEach((element) => {
  temp_users.push({
    userColor: ' ',
    userName: element['user']['nickname'],
    userID: element['id'],
    userRole: element['role'],
    userTotalWeekHour: element['total_week_hours'],
    scrumGoalSet: element['scrumgoal_set'].length,
  })
  if (element['user']['nickname'] == loggedUser) {
    loggedUserId = element['id'];
  }
  element['scrumgoal_set'].forEach((item) => {
    if (item['status'] == 0) {
      temp_TFTW.push({
        task: item['name'],
        taskFor: item['user'],
        goalID: item['goal_project_id'],
        timeCreated: item['time_created'],
        days_failed: item['days_failed'],
        file: item['file'],
      });
    }
    if (item['status'] == 1) {
      temp_TFTW.push({
        task: item['name'],
        taskFor: item['user'],
        goalID: item['goal_project_id'],
        timeCreated: item['time_created'],
        days_failed: item['days_failed'],
        file: item['file'],
      });
    }
    if (item['status'] == 2) {
      temp_verify.push({
        task: item['name'],
        taskFor: item['user'],
        goalID: item['goal_project_id'],
        pushID: item['push_id'],
        timeCreated: item['time_created'],
        days_failed: item['days_failed'],
        file: item['file'],
      });
    }
    if (item['status'] == 3) {
     temp_done.push({
        task: item['name'],
        taskFor: item['user'],
        goalID: item['goal_project_id'],
        pushID: item['push_id'],
        timeCreated: item['time_created'],
        days_failed: item['days_failed'],
        file: item['file'],
      });
    }
  });
});
setUsers(temp_users)
setTFTW(temp_TFTW)
console.log("Finish today")
console.log(TFTW)
setDone(temp_done)
setVerify(temp_verify)
}

  return (
<>
<body>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"></link>
{/* <div className="preloader">
  <img src="https://res.cloudinary.com/ros4eva/image/upload/v1576318446/Mac_h5coln.svg" id="loadrImg"></img>

</div> */}
<div className="page-content ">
  <div className="container-fluid">
    <div className="row">
      <nav>
        <div className="navbarr">
          <a className="navbar-brand" href="home"><img
              src="https://res.cloudinary.com/ros4eva/image/upload/v1576318445/images_iii9fe.png"
              style={{width: '116px', height: '26px'}}></img></a>
          <ul className="nav nav-tabs justify-content-end" id="myTab" role="tablist">
            <li className="nav-item">
              <a className={`nav-link ${addTaskAndNoteBtnVisible ? 'active' : ''}`} onClick={() => handleTabClick('home')} id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                aria-selected={addTaskAndNoteBtnVisible ? 'true' : 'false'} onClick={showAddTaskandNoteBTN}>My Tasks</a>
            </li>
            <li className="nav-item">
              <a className={`nav-link ${addTaskAndNoteBtnVisible ? '' : 'active'}`} id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                aria-selected={addTaskAndNoteBtnVisible ? 'false' : 'true'} onClick={hideAddTaskandNoteBTN}>Team Tasks</a>
            </li>
    
            <li className="nav-item">
              <div className="projectsDropDown" onClick={showProjectTabContents}>
                <a className="nav-link otherNavTools" id="projectsTab" role="tab">
                  <h4>Project</h4>
                  <span className="loggedProject">{loggedProject}</span>
                </a>
    
                <div id="projectsDDContent" ref={projectsDDContentref} className="projectsDropDownContent">
                  <p><span className="icon-check"></span>{loggedProject}</p>
                  <p className="clear_tft" style={{paddingLeft: '5px'}} hidden={loggedUserRole !== 'Owner'}>Auto Clear <bold style={{fontWeight: 'bold'}}>TFT</bold></p>
                  <p style={{paddingLeft: '5px', fontSize: '12px'}} hidden={loggedUserRole !== 'Owner'}>off<label className="switch">
                    <input id="auto_clear_tft" type="checkbox" onClick={autoClearTft}></input>
                    <span className="slider round"></span>
                  </label>on</p>
                  <div className="projectsDropDownAP" onClick={createNewProject}>
                    ADD PROJECT
                  </div>
                </div>
              </div>
    
            </li>
    
            <li className="nav-item">
              <div className="sprintDropDown" onClick={showSprintTabContents}>
                <a className="nav-link otherNavTools" id="sprintTab" role="tab">
                  <h4>Sprint {loggedSprint.sprintID}</h4>
                  <span className="loggedSprint">{moment(loggedSprint.dateCreated).format('MMM DD')} - 
                    {moment(loggedSprint.endDate).format('MMM DD')}</span>
                </a>
    
                <div id="sprintDDContent" ref={sprintDDContentref} className="sprintDropDownContent">
                  {/* <p><span className="icon-check"></span>
                      {loggedSprint.sprintID !== undefined && loggedSprint.sprintID !== ' ' && (<label className="activ"> Sprint
                      {loggedSprint.sprintID}:</label><span className="spanAct">{loggedSprint.dateCreated} -
                      {loggedSprint.endDate}</span>)}</p> */}
                      {currentSprint.map((sprint) => (
                        <p
                            key={sprint.sprintID}
                            sprintID={sprint.sprintID}
                            onClick={() =>
                            changeLoggedSprint(
                                sprint.sprintID,
                                sprint.dateCreated,
                                sprint.endDate
                            )
                            }
                        >
                            <label sprint-create-date={sprint.dateCreated}>
                            Sprint {sprint.sprintID}:
                            </label>
                            <span sprint-end-date={sprint.endDate}>
                            {new Date(sprint.dateCreated).toLocaleDateString()} -{' '}
                            {new Date(sprint.endDate).toLocaleDateString()}
                            </span>
                        </p>
                        ))}
                  <div className="sprintDropDownCS" id="createSprint" onClick={startNewSprint}>
                    NEW SPRINT
                  </div>
                </div>
              </div>
            </li>
    
            {usesSlack && (<div>
              <li className="nav-item">
                <a className="nav-link otherNavTools" id="slackTab" role="tab" onClick={connectSlack}>
                  <h4>Slack <img src="https://res.cloudinary.com/ros4eva/image/upload/v1582046655/Group_jinais.png" width="12"
                      height="12"></img></h4>
                  <span className="slackStatus">Connected</span>
                </a>
              </li>
            </div>)}

            {!usesSlack && (<div>
              <li className="nav-item" >
                <a className="nav-link otherNavTools" id="slackTab" role="tab" onClick={connectSlack}>
                  <h4>Slack <img src="https://res.cloudinary.com/ros4eva/image/upload/v1582046655/Group_jinais.png" width="12"
                    height="12"></img></h4> 
                  <span className="slackStatus text-danger">Not Connected !</span>
                </a>
              </li>
            </div>)}
    
    
            <li className="nav-item">
               <a className="nav-link" id="profileTab" role="tab"> {/*onClick={userProfileModal(addToUser, loggedUser, loggedUserRole);hideslackchat()"> */}
                <img src="https://res.cloudinary.com/ros4eva/image/upload/v1581678144/Ellipse_6_z5snsi.png"></img>
                <h4>{loggedUserProfile}</h4>
                <p>{loggedUserRole}</p>
              </a>
            </li>
    
            <li className="nav-item">
              <a className="nav-link" id="logoutTab" role="tab">
                <span className="icon-info-circle" onClick={appInfo}></span>
                <span className="icon-Logout" onClick={logoutModal}></span>
              </a>
            </li>
          </ul>
    
    
        </div>
    
      </nav>
    </div>
  </div>
  

  <div className="main ml-0 pl-0">
    <div className="container-fluid pr-0">
      <div className="row row-eq-height">
        <div className="splitleft col-md-9 pr-0 pl-0" id="splitLeft" ref={hidesref}>
          <span id="alert" ref={Alertref}></span>
    
          <div className="tab-content card-body" id="myTabContent" style={{position: 'relative'}}>
            {/* <!-- <div>Modal Box Start Here</div> --> */}
            <div className="openModalForTaskandNote">
              <button id="addTaskBtn" ref={addTaskBtnref} onClick={openAddTaskModal} className="btn openAddTaskModalBtn">ADD TASK</button>
              <button id="addNoteBtn" ref={addNoteBtnref} className="btn openAddNoteModalbtn" onClick={hideslackchat}><span
                  className="icon-Group-1"></span></button>
            </div>
    
            {/* <!-- <div>Modal Box For Adding Task </div> --> */}
            <div id="addTaskModal" ref={modaref} className="modal">
              <div className="modal-contentForTask">
                <div className="modal-header" style={{marginBottom: '7px'}}>
                  <img src="https://res.cloudinary.com/ros4eva/image/upload/v1576618715/Ellipse_cxegyr.svg"></img><br></br>
                  <span className="headerUserTitle">{loggedUser}</span>
                  <p className="headerUserRole">
                    {loggedUserRole}</p>
                  <span className="close " onClick={close}><span className="icon-cancel"></span></span>
    
                </div>
                <div className="modal-bodyForTask" >
                  <form name="addTaskForm" id="addTaskForm" onSubmit={submitAddTask}>
                    <span className="close ForMobile" onClick={close}><span className="icon-cancel"></span></span>
                    <span className="addT">Add A Task</span>
                    <textarea className="insidemodalContentForTask" name="goal_name" 
                      required></textarea>
                    <div className="submitTask">
                      <button type="submit" id="submitaddTaskBtn" className="btn addTbtn">ADD TASK</button>
                      <span className="btn closebtn" id="cancelAddTaskBtn" style={{fontWeight: 'bold'}}
                        onClick={close}>CANCEL</span>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            {/* <!---<div>Modal box for connecting to slack</div>--> */}
            <div className="modalforslack" id="slackModal">
              <div className="slackmodal-content">
                  <div className="slackmodal-header">
                    <span className="slackclose" onClick={closeSlackModal()}>&times;</span>
                    {/* <h4><img src="https://res.cloudinary.com/ros4eva/image/upload/v1582046655/Group_jinais.png" width="18"
                      height="15"> Slack Integration </img></h4>   */}
                  </div>
                  <div className="slackmodal-body">
                      <p>Slack is not yet integrated. You won't be able to send messages. Would you like to integrate slack now?</p>
                  </div>
                  <div className="slackmodal-footer d-flex justify-content-around">
                      <button className="button btn" onClick={closeSlackModal}>Cancel</button>
                      <span className="slackbtn btn-md" onClick={connectSlack} width="20"><p>Connect to Slack</p></span>
                  </div>
                  
              </div>
          </div>
    
            {/* <!-- <div>Modal Box For Adding Notes</div> -->
    
            <div id="addNoteModal" className="modal">
              <div className="modal-contentForNote">
                <div className="splitNoteModal upside">
                  <div className="modal-header" style={{marginBottom: 5px;">
                    <i className="fas fa-arrow-left ForMobile" onClick={close()"></i>
                    <img style={{marginTop: -5px; width: 24px;" src="https://res.cloudinary.com/ros4eva/image/upload/v1576618715/Ellipse_cxegyr.svg"><br>
                    <span className="headerUserTitle">{{loggedUser}}</span>
                    <p className="headerUserRole">{{loggedUserRole}}</p>
                    <span onClick={close()" className="icon-cancel close" style={{fontSize: 26px; marginTop: -22px;"></span>
                  </div>
    
                  <div style={{padding: 0px 12px; paddingLeft: 20px;">
                    <span className="addT addNT">Add Notes</span>
                    <form #addNoteForm name="addNote" id="addNote" (ngSubmit)="addNote()">
                      <textarea #noteToAdd="ngModel" [(ngModel)]="note_to_add" name="noteToAdd"
                        className="insidemodalContentForNote" style={{marginBottom: 15px;">
    
                                        </textarea>
                      <div className="addT" style={{marginTop: 0px;">
                        <span>Set Priority</span>
                      </div>
                      <div className="priorityI" style={{marginTop: 5px;">
                        <input type="radio" #priority="ngModel" name="priority" [(ngModel)]="notePriority" value="high">
                        <span>High</span>
                        <input type="radio" #priority="ngModel" name="priority" [(ngModel)]="notePriority"
                          value="medium"><span>Medium</span>
                        <input type="radio" #priority="ngModel" name="priority" [(ngModel)]="notePriority"
                          value="low"><span>Low</span>
                      </div>
    
                      <div className="submitNote" style={{marginBottom: 12px; marginTop: -15px;">
                        <button type="submit" id="addNoteBtn" className="btn addTbtn">ADD NOTE</button>
                        <span className="btn closebtn" id="cancelAddNote" style={{fontWeight: bold;"
                          onClick={close()">CANCEL</span>
                      </div>
                    </form>
                  </div>
                </div>
                <div className="splitNoteModal downside">
                  <div style={{padding: 0px 16px 2px 16px; background-color: white;">
                    <div className="userNoteHistory">
                      <div className="addT" style={{position: fixed;margin-left: -10px;  background-color: white; padding: 12px; width: 50%;">
                        <span className="addN">All Notes</span>
                      </div>
                      <div style={{padding: 10px;"></div>
                      <div *ngFor="let note of notes">
                        <div *ngIf="note['noteFor'] == addToUser">
                          <div className="tasks" style={{marginTop: 15px;">
                            <span className="h2">ID: </span><span className="h4">{{note['noteID']}}</span>
                            <br><span>{{note['note']}}</span>
                            <br><span #addNoteToTask [attr.note_to_task]="note['note']" [attr.note_id]="note['noteID']"
                              onClick={addNoteToUserTask(addNoteToTask)" style={{fontWeight: bold; cursor: pointer;"><i
                                className="fas fa-plus" style={{fontWeight: bolder; fontSize: 16px; margin-right: 5px;"></i>
                              Add To
                              Tasks</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> */}
    
            {/* <!-- <div>Modal For Task Edit</div> --> */}
            <div id="editTaskModal" ref={openEditTaskModalref} className="modal">
              <div className="modal-contentForTask">
                <div className="modal-header">
                  <img src="https://res.cloudinary.com/ros4eva/image/upload/v1576618715/Ellipse_cxegyr.svg"></img><br></br>
                  <span className="headerUserTitle">{loggedUser}</span>
                  <p className="headerUserRole">
                    {loggedUserRole}</p>
                  <span className="close" onClick={close}><span className="icon-cancel"></span></span>
    
                </div>
                <div className="modal-bodyForTask">
                  <form name="editTask" id="editUserTask" onSubmit={editTask}>
                    <span className="addT">Edit Task</span>
                    <textarea className="insidemodalContentForTask" aria-valuetext="taskToEdit"
                      name="taskToEdit">
                                    </textarea>
                    <div className="submitTask">
                      <button type="submit" id="editTaskBtn" className="btn addTbtn">EDIT
                        TASK</button>
                      <span className="btn closebtn" id="cancelAddTaskBtn" style={{fontWeight: 'bold'}}
                        onClick={close}>CANCEL</span>
                    </div>
                  </form>
                </div>
              </div>
            </div>
    
            {/* <!-- <div>Modal For Task Image Upload</div> -->
            <div id="uploadImageModal" className="modal">
              <div className="modal-contentForImage">
                <div className="modal-bodyForTask">
                  <div className="imgTop" style={{marginTop: 1em;">
                    <span className="addT">Upload Image</span>
                    <span className="close" onClick={close()" style={{marginTop: -8px;"><span className="icon-cancel"></span></span>
                  </div>
                  <form name="uploadImage" id="uploadImage" (ngSubmit)="imageUploadAlert()">
                    <label for="imgUpload" className="insidemodalContentForImageUpload">
                      <span className="imgMsg">{{imgName}}</span>
                    </label>
                    <div className="fileName">CHOOSE FILE</div>
                    <input type="file" name="image_uploaded" id="imgUpload" accept="image/*" (change)="imageName()">
                    <div className="uploadImg">
                      <button type="submit" id="uploadImageBtn" className="btn addTbtn">UPLOAD</button>
                    </div>
    
                    <div className="progressTrack">
                      <div id="progressBar" className="progressBar"></div>
                    </div>
                  </form>
    
                </div>
              </div>
            </div> */}
    
            {/* <!-- <div>Modal Box For Task History</div> -->
    
            <div id="taskHistoryModal" className="modal">
              <div className="modal-contentForTaskHistory">
                <div className="taskHistoryModalSplit">
                  <div className="taskHistorySide">
                    <div className="container" style={{padding: 20px;">
                      <div *ngFor="let historyFor of history_for">
                        <div *ngIf="historyFor">
                          <div className="ex_taskHistory">
                            <div className="excontent" style={{border: 1px solid rgba(0, 0, 0, 0.1); border-radius: 10px;">
                              <div className="task_text">
                                {{historyFor['history']}}
                              </div><br>
                              <div className="uploadedImage" *ngIf="historyFor['file'];">
                                <img #viewClickedImage [attr.image_to_view]="'image' + historyFor['historyProjectID']"
                                  src="{{dataService.imageApi}}{{historyFor['file']}}"
                                  onClick={userImageModal(viewClickedImage); hideslackchat()">
                              </div>
                              <div className="scrumtools">
                                <n style={{margin-right: 8px;">{{historyFor['historyProjectID']}}</n>
                                <span className="icon-pencil tools" alt="edit" title="edit"></span>
                                <span className="icon-camera tools" alt="upload image" title="upload image"></span><i className="fas fa-clone tools"
                                  alt="copy" title="copy" style={{fontSize: 15px;"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
    
                  <div className="taskHistorySide">
                    <div className="container singleTask">
                      <div className="header">
                        <h4>Single Task History</h4>
                        <span className="close" onClick={close()" style={{margin-right: -10px; marginTop: -10px;"><span style={{fontSize: 25px;" className="icon-cancel"></span></span>
                      </div>
                      <div className="allTaskContainer">
                        <div *ngFor="let history of goal_history">
                          <div *ngIf="history">
                            <div className="goal_moved">
                              <div className="goal_moved_head">
                                <i className="fas fa-circle"></i><span className="move_heading">{{history['historyMessage']}}</span><br>
                                <label className="label">By</label><span className="label" style={{color: #26A69A;">
                                  {{history['historyMovedBy']}}</span>
                              </div>
                              <div className="move_info" style={{marginTop: 15px; marginBottom: 40px;">
                                <div className="move_status status_container">
                                  <span className="status_and_time">Status</span>
                                  <p>{{history['historyStatus']}}</p>
                                </div>
                            
                                <div className="move_status status_container">
                                  <span className="status_and_time">Hours</span>
                                  <p>{{history['historyHours']}}</p>
                                </div>
                            
                                <div className="move_status status_container">
                                  <span className="status_and_time">Date</span>
                                  <p>{{history['timeCreated'] | date: 'MMMM dd, yyyy'}}</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
    
                        <!-- <div style={{padding: 10px;"></div> -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> */}
    
            {/* <!-- <div>Modal Box For User Profile</div> -->
    
            <div id="userProfileModal" className="modal">
              <div className="modal-contentForUserProfile">
                <div className="splitUPM">
                  <div className="splitUPMLeftt">
                    <div className="splitUPMLeftHeader">
                      <div className="container">
                        <img src="https://res.cloudinary.com/ros4eva/image/upload/v1581554201/Ellipse_5_rzifrh.png"
                          width="22" height="22">
                        <span className="headerUserTitle">{{historyForUser}}</span>
                        <p className="headerUserRole">
                          {{historyForUserRole}}</p>
                      </div>
                    </div>
                    <div className="container">
                      <div style={{padding:25px;"></div>
                      <div *ngFor="let userSprint of currentSprint">
                        <div *ngIf="userSprint">
                          <div className="splitUPMLeftContent">
                            <div className="container" onClick={userTaskHistoryForSprint(userSprint['sprintID'], addToUser)">
                              <h5>Sprint {{userSprint['sprintID']}}:</h5>
                              <span>{{userSprint['dateCreated']| date: 'MMM dd, yyy'}} -
                                {{userSprint['endDate']| date:'MMM dd, yyy'}}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div style={{padding: 10px"></div>
                    </div>
                  </div>
                  <div className="splitUPMLeftt">
                    <div className="container">
                      <div className="userHistories">
                        <div className="userHistoryHead">
                          <h3>User Task Histories</h3>
                          <span style={{fontSize: 28px; margin-right: -5px;" onClick={close()" className="icon-cancel"></span>
                        </div>
                        <div *ngFor="let thisSprintTask of clicked_task_history">
                          <div *ngIf="thisSprintTask">
                            <div className="userHistoryTasks">
                              <div>
                                <span className="labelForWorkID">Work ID: </span>
                                <span className="workID" *ngIf="thisSprintTask['pushID'] != 'Null Value'">{{thisSprintTask['pushID']}}</span>
                                <span className="workID" *ngIf="thisSprintTask['pushID'] == 'Null Value'">None</span>
                              </div>
                              <div style={{marginTop: -7px!important;">
                                <span className="labelForWorkIDStatus">Status: </span><span className="status statusNotCompleted" *ngIf="thisSprintTask['status'] != '3'" style={{color: #FF4D4D;">Not
                                  Completed</span><span className="status statusNotCompleted" *ngIf="thisSprintTask['status'] == '3'" style={{color:#4DFFD4;">Completed</span>
                                
                              </div>
                              <p>{{thisSprintTask['task']}}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
    
              </div>
            </div> */}
    
            {/* <!-- <div>Modal Box For App Info</div> -->
    
            <div id="appInfoModal" className="modal">
              <div className="modal-contentForAppInfo">
                <span className="close" onClick={close()" style={{margin: 10px;"><span className="icon-cancel"></span></span>
                <div className="container">
                  <div className="appVersion">
                    <img src="https://res.cloudinary.com/ros4eva/image/upload/v1581797659/Rectangle_3_zphwex.png">
                    <h3>ChatScrum</h3>
                    <span>v1.2.0012</span>
    
                  </div>
                  <div className="newFeatures">
                    <h4>New Features</h4>
                    <p>- Sea cattle.</p>
                    <p>- The moving together there don't two night our midst</p>
                    <p>- Behold there fill brought tree rule there from firmament night be female.</p>
                    <p>- Firmament he replenish third fruit</p>
                    <p>- Him there grass beast. Very wherein.</p>
                    <p>- Hath, them.</p>
                    <p>- Seas grass shall form had divide.</p>
                    <h4 className="NFSupport">Support</h4>
                    <span>For support, send a mail to info@linuxjobber.com</span>
                  </div>
                  <button className="closeNewFeatureBtn" onClick={close()">CLOSE</button>
                </div>
              </div>
            </div> */}
    
            {/* <div>Modal Box For Logout</div> */}
    
            <div id="logoutModal" ref={logoutModalref} className="modal">
              <div className="modal-contentForLogout">
                <span className="close" onClick={close} style={{margin: '10px'}}><span className="icon-cancel"></span></span>
    
                <div className="signOutText">
                  <span>Sign Out?</span>
                </div>
                <div className="submitSignOutRequest">
                  <button type="button" id="signOutBtn" className="btn addTbtn" style={{fontSize: '14px'}} onClick={logout}>SIGN
                    OUT
                  </button>
                  <button className="btn closebtn" id="cancelsignOutBtn" style={{fontWeight: 'bold'}}
                    onClick={close}>CANCEL</button>
                </div>
    
              </div>
            </div>
    
            {/* <!--Modal Box For Upload Image View-->
    
            <div id="uploadedImageModal" className="image_modal">
              <img className="modal-contentuploadedImage" id="imageToView">
            </div> */}
    
            {/* <!-- Modal Box For Push Id --> */}
            <div id="dialog" title="Please Confirm?" ref={dialogref}>
              <div className="container">
                <form onSubmit={processMoveGoalRequest, closeDialog}>
                  <label for="push_id"
                //   Sanserrif excluded
                    style={{fontSize: '12px', color: 'rgba(0, 0, 0, 0.8)', fontFamily: 'roboto', marginBottom: '-5px', marginTop: '-5px'}}>Push
                    ID:</label>
                  <input maxlength="10" type="text" name="push_id"
                    style={{width: '100%', padding: '3px 14px', borderRadius: '4px', border: '1px solid transparent', backgroundColor: 'rgba(0, 0, 0, 0.1)', outlineColor:'transparent', fontSize: '14px'}}>
                  </input>
                  {/* {pushID && (<small style={{color: 'rgba(0, 0, 0, 0.4)', fontSize: '12px'}> Max 10
                    Characters: {{pushID.length || 0}}/10</small>)} */}
                  <br></br>
                  <label for="hours"
                    style={{fontSize: '12px', color: 'rgba(0, 0, 0, 0.8)', fontFamily: 'roboto' , marginBottom: '-5px'}}>Hours
                    Spent:</label>
                  <input type="number" name="hours"
                  style={{width: '100%', padding: '3px 14px', borderRadius: '4px', border: '1px solid transparent', backgroundColor: 'rgba(0, 0, 0, 0.1)', outlineColor:'transparent', fontSize: '14px'}}>
                  </input>
                  <button type="submit"
                    style={{padding: '4px 7px', border: 'none', background: 'rgba(0, 0, 0, 0.1)', marginTop: '5px', fontSize: '12px', outlineColor: 'rgba(0, 0, 0, 0.1)', borderRadius: '4px'}}>Move
                    Goal</button>
                </form>
            
              </div>
            </div>
    
            {/* <!-- Modal Box For Changing User Role -->
    
            <div id="changeUserRoleModal" className="modal">
              <div className="modal-contentForTask">
                <div className="modal-header" style={{marginBottom: 7px;">
                  <img src="https://res.cloudinary.com/ros4eva/image/upload/v1576618715/Ellipse_cxegyr.svg"><br>
                  <span className="headerUserTitle">{{loggedUser}}</span>
                  <p className="headerUserRole">
                    {{loggedUserRole}}</p>
                  <span className="close " onClick={close()" style={{marginTop: -20px;"><span className="icon-cancel"></span></span>
            
                </div>
                <div className="modal-bodyForTask" style={{background: #FAFAFA; marginTop: -6px; padding-bottom: 20px;">
                  <form name="changeUserRoleForm" id="changeUserRoleForm" (ngSubmit)="submitchangeUserRole()">
                    <span className="close ForMobile" onClick={close()"><span className="icon-cancel"></span></span>
                    <span className="addT">Change User Role</span>
                    <p className="choose_role">Choose between Developer, Admin, Quality Analyst or Owner.</p>
                    <select [(ngModel)]="new_role" name="new_role" className="custom-select">
                      <option value="admin">Admin</option>
                      <option value="developer">Developer</option>
                      <option value="owner">Owner</option>
                      <option value="quality analyst">Quality Analyst</option>
                    </select>
                    <div className="submitTask">
                      <button type="submit" id="changeUserRoleBtn" className="btn addTbtn" style={{width: 165px;">CHANGE</button>
                      <span className="btn closebtn" id="cancelAddTaskBtn" style={{fontWeight: bold;" onClick={close()">CANCEL</span>
                    </div>
                  </form>
                </div>
                <div className="remove_user btn" onClick={deleteUser()">
                  REMOVE USER
                </div>
              </div>
            </div> */}
    
            {/* <!-- <div>Modal Box Stops Here</div> --> */}
            
            <div className="container">
              <div id="sprintAlert" ref={sprintAlertref} className="sprintAlert" style={{marginTop: '-20px',marginBottom: '10px'}}>
                <strong className="mr-auto">Hello! {loggedUser}, </strong>Would you like to Start a new Sprint? <bold
                  onClick={startNewSprint, sprintAlertHidden}>Yes </bold>
                <bold onClick={sprintAlertHidden}>No</bold>
              </div>
            </div>
            
            <div className={`tab-pane ${addTaskAndNoteBtnVisible ? 'show fade active' : ''}`} id="home" role="tabpanel" aria-labelledby="home-tab">
              <div className="container">
                <div className="row">
                  <div className="col">
                    <div className="ex" id="tfw">
                      <h4>Tasks for the week</h4>
                      <div 
                        className="example-list" 
                        ref={tftwList}
                        data-cdk-drop-list-data={TFTW}
                        data-cdk-drop-list-connected-to={[tftdList, verifyList, doneList]}
                        onDrop={(event) => drop(event)}
                        id={`${loggedUserId}e0`}
                      >
                        {TFTW.map(item => {
                          if (item.taskFor === loggedUserId && item.task !== '' && item.timeCreated >= loggedSprint.dateCreated && loggedSprint.endDate >= item.timeCreated) {
                            return (
                              <div key={item.id} className="draggable-item" draggable="true" onDragStart={() => handleDragStart(item)}>
                                <div className="dragged_item_placeholder"><span className="icon-plus-symbol" style={{ fontSize: '32px' }} /></div>
                                <div className="excontent" style={{ border: '1px solid rgba(0, 0, 0, 0.1)' }}>
                                  <div className="task_days_failed" hidden={item.days_failed === '0'}>{item.days_failed}</div>
                                  <div className="task_text" style={{ cursor: 'move' }}>
                                    {item.task}
                                  </div>
                                  <br />
                                </div>
                              </div>
                            );
                          } else {
                            return null;
                          }
                        })}
                        </div>
                        </div>
                        </div>

                        <div class="col ">
                    <div class="ex" id="tft">
                      <h4>Todays Target</h4>
                      </div>
                      </div>

                      <div class="col">
                    <div class="ex" id="verify">
                      <h4>Verify Task</h4>
                      </div>
                      </div>

                      <div class="col">
                    <div class="ex" id="done">
                      <h4 style={{marginBottom: '7px'}}>Done</h4>
                      </div>
                      </div>

                        </div>
                        </div>
                        </div>
        {/* {TFTW.map((item) => {
        if (item['taskFor'] == loggedUserId && item['task'] != '' && item['timeCreated'] >= loggedSprint['dateCreated'] && loggedSprint['endDate'] >= item['timeCreated']) {
          return (
            <div key={item['goalID']}
              classNameName="cdk-drag"
              data-cdk-drag-data={item}
            >
              <div classNameName="dragged_item_placeholder" data-cdk-drag-placeholder><span classNameName="icon-plus-symbol" style={{{fontSize: "32px"}}></span></div>
              <div classNameName="excontent" style={{{border: "1px solid rgba(0, 0, 0, 0.1)"}}>
                <div classNameName="task_days_failed" hidden={item['days_failed'] == '0'}>{item['days_failed']}</div>
                <div style={{{cursor: "move"}} classNameName="task_text">
                  {item['task']}
                </div>
                <br />
                {item['file'] &&
                  <div classNameName="uploadedImage">
                    <img ref={viewClickedImage} image_to_view={'image' + item['goalID']} src={`${dataService.domain_protocol}${dataService.domain_name}${item['file']}`} onClick={() => {userImageModal(viewClickedImage); hideslackchat();}}/>
                    <div classNameName="excontent" style={{{border: "1px solid rgba(0, 0, 0, 0.1)"}} data-cdk-drag-preview>
                      <div classNameName="task_days_failed" hidden={item['days_failed'] == '0'}>{item['days_failed']}</div>
                      <div style={{{cursor: "move"}} classNameName="task_text">
                        {item['task']}
                      </div>
                      <br />
                      <div classNameName="scrumtools">
                        <n style={{{marginRight: "8px"}}>{item['goalID']}</n>
                        <span ref={loggedUserId} task_to_edit={item['task']} task_id_to_edit={item['goalID']} classNameName="icon-pencil tools" alt="edit" title="edit" onClick={() => editTaskModal(loggedUserId)}></span>
                        <span ref={loggedUserUploadImage} task_id_to_upload_img={item['goalID']} classNameName="icon-camera tools" alt="upload image" title="upload image" onClick={() => uploadImage(loggedUserUploadImage)}></span>
                        <i classNameName="fas fa-clone tools" alt="copy" title="copy" style={{{fontSize: "15px"}} onClick={() => copyToClipboard(loggedUserId)}></i>
                        <i classNameName="fas fa-envelope tools" alt="task chat" title="task chat"></i>
                        <span classNameName="icon-history-clock-button tools" alt="history" title="history" id="taskHistory" onClick={() => {taskHistory(); viewTaskHistory(item['goalID']); hideslackchat();
                                    </div>
                                  </div>
                                  <!--Drag preview Ends-->
                              </div>
    
                              <div className="scrumtools">
                                <n style={{margin-right: 8px;">{goalID}</n>
                                <span 
                                className="icon-pencil tools" alt="edit" title="edit"
                                  onClick={editTaskModal(loggedUserId)}></span>
                                <span
                                  className="icon-camera tools" alt="upload image" title="upload image"
                                  onClick={uploadImage(loggedUserUploadImage)}></span><i className="fas fa-clone tools"
                                  alt="copy" title="copy" style={{fontSize: 15px;"
                                  onClick={copyToClipboard(loggedUserId)}></i>
                                <i className="fas fa-envelope tools" alt="task chat" title="task chat"></i>
                                <span alt="history" title="history" id="taskHistory" className="icon-history-clock-button tools" onClick={taskHistory, viewTaskHistory(item['goalID']), hideslackchat}></span>
                                <span className="icon-move tools" alt="move" title="move"></span>
                              </div>
          )}
                            </div>
                            
                            
                          </div>
                        </div>
                      </div>
                    </div>
    
                  </div>
                  <div className="col ">
                    <div className="ex" id="tft">
                      <h4>Todays Target</h4>
                      <div
                        className="example-list"
                        cdkDropList #tftdList="cdkDropList"
                        [cdkDropListData]="TFTD"
                        [cdkDropListConnectedTo]="[tftwList, verifyList, doneList]"
                        (cdkDropListDropped)="drop($event)"
                        id="{{loggedUserId}}e1"
                      >
                        <div *ngFor="let item of TFTD"  cdkDrag [cdkDragData]="item">
                          <div
                            *ngIf="item['taskFor'] == loggedUserId && item['task'] != '' && item['timeCreated'] &#62;= loggedSprint['dateCreated'] && loggedSprint['endDate'] &#62;= item['timeCreated']">
                            <div className="dragged_item_placeholder" *cdkDragPlaceholder><span className="icon-plus-symbol" style={{fontSize: 32px;"></span></div>
                            <div className="excontent" style={{border: 1px solid rgba(0, 0, 0, 0.1);">
                              <div className="task_days_failed" [hidden]="item['days_failed'] == '0'">{{item['days_failed']}}</div>
                              <div style={{cursor: move;" className="task_text">
                                {{item['task']}}
                              </div><br>
                              <div className="uploadedImage" *ngIf="item['file']"> 
                                <img #viewClickedImage [attr.image_to_view]="'image' + item['goalID']"
                                  src="{{dataService.domain_protocol}}{{dataService.domain_name}}{{item['file']}}"
                                  onClick={userImageModal(viewClickedImage), hideslackchat}>
                                  <!--Drag Preview-->
                                  <div className="excontent" style={{border: 1px solid rgba(0, 0, 0, 0.1);" *cdkDragPreview>
                                    <div className="task_days_failed" [hidden]="item['days_failed'] == '0'">{{item['days_failed']}}</div>
                                    <div style={{cursor: move;" className="task_text">
                                      {{item['task']}}
                                    </div><br>
                                        
                                    <div className="scrumtools">
                                      <n style={{margin-right: 8px;">{{item['goalID']}}</n>
                                      <span #loggedUserId [attr.task_to_edit]="item['task']"
                                        [attr.task_id_to_edit]="item['goalID']" className="icon-pencil tools" alt="edit" title="edit"
                                        onClick={editTaskModal(loggedUserId)"></span>
                                      <span #loggedUserUploadImage [attr.task_id_to_upload_img]="item['goalID']"
                                        className="icon-camera tools" alt="upload image" title="upload image"
                                        onClick={uploadImage(loggedUserUploadImage)"></span><i className="fas fa-clone tools"
                                        alt="copy" title="copy" style={{fontSize: 15px;"
                                        onClick={copyToClipboard(loggedUserId)"></i>
                                      <i className="fas fa-envelope tools" alt="task chat" title="task chat"></i>
                                      <span alt="history" title="history" id="taskHistory" className="icon-history-clock-button tools" onClick={taskHistory(); viewTaskHistory(item['goalID']); hideslackchat()"></span>
                                      <span className="icon-move tools" alt="move" title="move"></span>
                                    </div>
                                  </div>
                                  <!--Drag preview Ends-->
                               </div> 
    
                              <div className="scrumtools">
                                <n style={{margin-right: 8px;">{{item['goalID']}}</n>
                                <span #loggedUserId [attr.task_to_edit]="item['task']"
                                  [attr.task_id_to_edit]="item['goalID']" className="icon-pencil tools" alt="edit" title="edit"
                                  onClick={editTaskModal(loggedUserId)"></span>
                                <span #loggedUserUploadImage [attr.task_id_to_upload_img]="item['goalID']"
                                  className="icon-camera tools" alt="upload image" title="upload image"
                                  onClick={uploadImage(loggedUserUploadImage)"></span><i className="fas fa-clone tools"
                                  alt="copy" title="copy" style={{fontSize: 15px;"
                                  onClick={copyToClipboard(loggedUserId)"></i>
                                <i className="fas fa-envelope tools" alt="task chat" title="task chat"></i>
                                <span alt="history" title="history" id="taskHistory" className="icon-history-clock-button tools"
                                  onClick={taskHistory(); viewTaskHistory(item['goalID']); hideslackchat()"></span>
                                <span className="icon-move tools" alt="move" title="move"></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> */}
                  {/* <div className="col">
                    <div className="ex" id="verify">
                      <h4>Verify Task</h4>
                      <div
                        className="example-list"
                        cdkDropList #verifyList="cdkDropList"
                        [cdkDropListData]="verify"
                        [cdkDropListConnectedTo]="[tftwList, tftdList, doneList]"
                        (cdkDropListDropped)="drop($event)"
                        id="{{loggedUserId}}e2"
                      >
                        <div *ngFor="let item of verify" cdkDrag [cdkDragData]="item">
                          <div
                            *ngIf="item['taskFor'] == loggedUserId && item['task'] != '' && item['timeCreated'] &#62;= loggedSprint['dateCreated'] && loggedSprint['endDate'] &#62;= item['timeCreated']">
                            <div className="dragged_item_placeholder" *cdkDragPlaceholder><span className="icon-plus-symbol" style={{fontSize: 32px;"></span></div>
                            <div className="excontent" style={{border: 1px solid rgba(0, 0, 0, 0.1);">
                              <div className="task_days_failed" [hidden]="item['days_failed'] == '0'">{{item['days_failed']}}</div>
                              <div style={{cursor: move;" className="task_text">
                                {{item['task']}}
                              </div><br>
                              <div className="uploadedImage" *ngIf="item['file']">
                                <img #viewClickedImage [attr.image_to_view]="'image' + item['goalID']"
                                  src="{{dataService.domain_protocol}}{{dataService.domain_name}}{{item['file']}}" onClick={userImageModal(viewClickedImage); hideslackchat()">
                                  <!--Drag Preview-->
                                  <div className="excontent" style={{border: 1px solid rgba(0, 0, 0, 0.1);" *cdkDragPreview>
                                    <div className="task_days_failed" [hidden]="item['days_failed'] == '0'">{{item['days_failed']}}</div>
                                    <div style={{cursor: move;" className="task_text">
                                      {{item['task']}}
                                    </div><br>
                                        
                                    <div className="scrumtools">
                                      <n style={{margin-right: 8px;">{{item['goalID']}}</n>
                                      <span #loggedUserId [attr.task_to_edit]="item['task']"
                                        [attr.task_id_to_edit]="item['goalID']" className="icon-pencil tools" alt="edit" title="edit"
                                        onClick={editTaskModal(loggedUserId)"></span>
                                      <span #loggedUserUploadImage [attr.task_id_to_upload_img]="item['goalID']"
                                        className="icon-camera tools" alt="upload image" title="upload image"
                                        onClick={uploadImage(loggedUserUploadImage)"></span><i className="fas fa-clone tools"
                                        alt="copy" title="copy" style={{fontSize: 15px;"
                                        onClick={copyToClipboard(loggedUserId)"></i>
                                      <i className="fas fa-envelope tools" alt="task chat" title="task chat"></i>
                                      <span alt="history" title="history" id="taskHistory" className="icon-history-clock-button tools" onClick={taskHistory(); viewTaskHistory(item['goalID']); hideslackchat()"></span>
                                      <span className="icon-move tools" alt="move" title="move"></span>
                                    </div>
                                  </div>
                                  <!--Drag preview Ends-->
                              </div>
                        
                              <div className="scrumtools">
                                <n style={{margin-right: 8px;">{{item['goalID']}}</n>
                                <span #loggedUserId [attr.task_to_edit]="item['task']" [attr.task_id_to_edit]="item['goalID']"
                                  className="icon-pencil tools" alt="edit" title="edit" onClick={editTaskModal(loggedUserId)"></span>
                                <span #loggedUserUploadImage [attr.task_id_to_upload_img]="item['goalID']" className="icon-camera tools"
                                  alt="upload image" title="upload image" onClick={uploadImage(loggedUserUploadImage)"></span><i
                                  className="fas fa-clone tools" alt="copy" title="copy" style={{fontSize: 15px;"
                                  onClick={copyToClipboard(loggedUserId)"></i>
                                <i className="fas fa-envelope tools" alt="task chat" title="task chat"></i>
                                <span alt="history" title="history" id="taskHistory" className="icon-history-clock-button tools"
                                  onClick={taskHistory(); viewTaskHistory(item['goalID']); hideslackchat()"></span>
                                <span className="icon-move tools" alt="move" title="move"></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col">
                    <div className="ex" id="done">
                      <!-- <div style={{margin-left: 10px;"> -->
                      <h4 style={{marginBottom: 7px;">Done</h4>
                      <div className="child"  style={{position: relative;">
                        <div>
                          <div
                            className="example-list"
                            cdkDropList #doneList="cdkDropList"
                            [cdkDropListData]="done"
                            [cdkDropListConnectedTo]="[tftwList, tftdList, verifyList]"
                            (cdkDropListDropped)="drop($event)"
                            id="{{loggedUserId}}e3"
                          >
                            <div *ngFor="let item of done"  cdkDrag [cdkDragData]="item">
                              <div
                                *ngIf="item['taskFor'] == loggedUserId && item['task'] != '' && item['timeCreated'] &#62;= loggedSprint['dateCreated'] && loggedSprint['endDate'] &#62;= item['timeCreated']">
                                <div className="dragged_item_placeholder" *cdkDragPlaceholder><span className="icon-plus-symbol" style={{fontSize: 32px;"></span></div>
                                <div style={{position: absolute; left: 8px; marginTop: -25px;">
                                  <span style={{fontWeight: bold; fontSize: 13px; font-family: 'roboto',sans-serif;">{{item['pushID']}}</span>
                                </div>
                            
                                <div className="excontent" style={{border: 1px solid rgba(0, 0, 0, 0.1); marginTop: 35px;">
                                  <div className="task_days_failed" [hidden]="item['days_failed'] == '0'">{{item['days_failed']}}</div>
                                  <div style={{cursor: move;" className="task_text">
                                    {{item['task']}}
                                  </div><br>
                                  <div className="uploadedImage" *ngIf="item['file']">
                                    <img #viewClickedImage [attr.image_to_view]="'image' + item['goalID']"
                                      src="{{dataService.domain_protocol}}{{dataService.domain_name}}{{item['file']}}" onClick={userImageModal(viewClickedImage); hideslackchat()">
                                      <!--Drag Preview-->
                                  <div className="excontent" style={{border: 1px solid rgba(0, 0, 0, 0.1);" *cdkDragPreview>
                                    <div className="task_days_failed" [hidden]="item['days_failed'] == '0'">{{item['days_failed']}}</div>
                                    <div style={{cursor: move;" className="task_text">
                                      {{item['task']}}
                                    </div><br>
                                        
                                    <div className="scrumtools">
                                      <n style={{margin-right: 8px;">{{item['goalID']}}</n>
                                      <span #loggedUserId [attr.task_to_edit]="item['task']"
                                        [attr.task_id_to_edit]="item['goalID']" className="icon-pencil tools" alt="edit" title="edit"
                                        onClick={editTaskModal(loggedUserId)"></span>
                                      <span #loggedUserUploadImage [attr.task_id_to_upload_img]="item['goalID']"
                                        className="icon-camera tools" alt="upload image" title="upload image"
                                        onClick={uploadImage(loggedUserUploadImage)"></span><i className="fas fa-clone tools"
                                        alt="copy" title="copy" style={{fontSize: 15px;"
                                        onClick={copyToClipboard(loggedUserId)"></i>
                                      <i className="fas fa-envelope tools" alt="task chat" title="task chat"></i>
                                      <span alt="history" title="history" id="taskHistory" className="icon-history-clock-button tools" onClick={taskHistory(); viewTaskHistory(item['goalID']); hideslackchat()"></span>
                                      <span className="icon-move tools" alt="move" title="move"></span>
                                    </div>
                                  </div>
                                  <!--Drag preview Ends-->
                                  </div>
                                  <div className="scrumtools">
                                    <n style={{margin-right: 8px;">{{item['goalID']}}</n>
                                    <span #loggedUserId [attr.task_to_edit]="item['task']" [attr.task_id_to_edit]="item['goalID']"
                                      className="icon-pencil tools" alt="edit" title="edit" onClick={editTaskModal(loggedUserId)"></span>
                                    <span #loggedUserUploadImage [attr.task_id_to_upload_img]="item['goalID']" className="icon-camera tools"
                                      alt="upload image" title="upload image" onClick={uploadImage(loggedUserUploadImage)"></span><i
                                      className="fas fa-clone tools" alt="copy" title="copy" style={{fontSize: 15px;"
                                      onClick={copyToClipboard(loggedUserId)"></i>
                                    <i className="fas fa-envelope tools" alt="task chat" title="task chat"></i>
                                    <span alt="history" title="history" id="taskHistory" className="icon-history-clock-button tools"
                                      onClick={taskHistory(); viewTaskHistory(item['goalID']); hideslackchat()"></span>
                                    <span className="icon-move tools" alt="move" title="move"></span>
                                    <i style={{margin-right: 0px!Important; margin-left: 0px!Important;" className="fas fa-trash tools" alt="delete" title="delete" onClick={deleteTask(item['goalID'], item['task'])" [hidden]="loggedUserRole == 'Developer'"></i>
                                  </div>
                                </div>
                              </div>
                            
                            </div>
                          </div>
    
                        </div>
                      </div>
                      <!-- </div> -->
                    </div>
                  </div>
                </div> */}
                <div style={{marginTop:'5px', padding: '15px 0px'}}>
                </div>
              </div>
            </div>
              {/* This is where team tasks go */}
              <div className={`tab-pane ${!addTaskAndNoteBtnVisible ? 'show fade active' : ''}`} onClick={() => handleTabClick('profile')} id="profile" role="tabpanel" aria-labelledby="profile-tab">
              <div class="container">
                {users.map((user) => {
                  <>
                  <div className="teamTaskDropDownMenu" style={{background:'rgba(0, 0, 0, 0.1)'}} id={user.userID}>
                  <div>
                            <img src="https://res.cloudinary.com/ros4eva/image/upload/v1581417531/Ellipse_2_ithyej.png"
                            style={{border:'1px solid rgba(0, 0, 0, 0.1)'}}
                            ></img>
                            <ul class="ttUserProfile">
                              <li ><span class="teamTaskTitle" style={{color:'rgba(0, 0, 0, 0.1)'}}>{user.userName}</span></li>
                              <li style={{marginTop: '-4px'}} class="ttTasksandTime">{user.userTotalWeekHour} hours | {user.scrumGoalSet} Tasks
                              </li>
                            </ul>
                          </div>
                  </div>
                  </>
                })
                }
              </div>
              </div>
          </div>
        </div>
    
        {/* <!-- modal for websocket chat     -->
        <div className="card">
          <div className="card-header msg_head">
              <span>Scrum Chat</span>
              <p>Messages</p>
          </div>
          <div className="card-body msg_card_body" #scrollMe [scrollTop]="scrollMe.scrollHeight">
                  <div *ngFor="let message of my_messages">
                      <div className="d-flex justify-content-start mb-4" *ngIf= "message.username !== username">
                          <div className="img_cont_msg">
                              <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" alt="" className="rounded-circle user_img_msg">
                          </div>
                          <div className="msg_container">
                            <b>{{message.username}}:</b> &nbsp; {{message.message || message.content}}
                            <span className="msg_time">{{message.timestamp}}</span>
                          </div>
                      </div>
                      <div className="d-flex justify-content-end mb-4" *ngIf= "message.username == username">
                          <div className="msg_container_send">
                              {{message.message || message.content}}
                              <span className="msg_time_send">{{message.timestamp}}</span>
                          </div>
                      </div>
                  </div>
          </div>
          <div className="card-footer">
              <form #sendForm = "ngForm">
                  <div className="input-group">
                      <!-- <textarea name="chat_text" className="form-control type_msg" placeholder="Type your message..." ngModel></textarea> -->
                      <textarea className="form-control type_msg" [mention]="listUsers" [mentionConfig]="mentionConfig" [(mentioned-user)]="mention.item" placeholder="Send a message" #chattext= "ngModel" [(ngModel)]= "chat_text" name=chattext></textarea>
                      <div className="input-group-append" onClick={sendAMessage(sendForm)">
                          <span className="input-group-text send_btn"><i className="fas fa-location-arrow"></i></span>
                      </div>
                  </div>
              </form>
          </div>
        </div> */}
        {/* <!-- modal for websocket chat end --> */}
      </div>
    </div>

  {/* <div className="slackNotification">
            <span>Joshua Okwe has joined the chat</span>
          </div>
          <div className="container slackMessage">
            <div className="userName">
              <img src="https://res.cloudinary.com/ros4eva/image/upload/v1581554201/Ellipse_5_rzifrh.png">
              <h3>Joshua Okwe</h3>
            </div>
            <div className="userMsg">
              <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>
            </div>
            <div className="msgTime">
              <p>10:02 am • 9-07-2019</p>
            </div>
          </div>

          <div className="container slackMessage">
            <div className="userName">
              <img src="https://res.cloudinary.com/ros4eva/image/upload/v1581417531/Ellipse_2_ithyej.png">
              <h3 style={{color: #FF4D6D;">Azeem Animashaun</h3>
            </div>
            <div className="userMsg">
              <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna
                aliqua.</span>
            </div>
            <div className="msgTime">
              <p>10:02 am • 9-07-2019</p>
            </div>
          </div>

          <div className="slackNotification">
            <span>Louis E has joined the chat</span>
          </div>

          <div className="container slackMessage">
            <div className="userName">
              <img src="https://res.cloudinary.com/ros4eva/image/upload/v1581553992/Ellipse_3_fb4kdi.svg">
              <h3 style={{color: #59BB1E;">Louis E</h3>
            </div>
            <div className="userMsg">
              <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna
                aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore et dolore
                magna aliqua.</span>
            </div>
            <div className="msgTime">
              <p>10:02 am • 9-07-2019</p>
            </div>
          </div>

          <div className="slackNotification">
            <span>Azeem Animashaun has left the chat</span>
          </div>

          <div className="slackNotification">
            <span>Olufeko Samuel has joined the chat</span>
          </div>

          <div className="slackNotification">
            <span>Oluwaseyi.E has left the chat</span>
          </div>

          <div className="container slackMessage">
            <div className="userName">
              <img src="https://res.cloudinary.com/ros4eva/image/upload/v1581554201/Ellipse_5_rzifrh.png">
              <h3>Joshua Okwe</h3>
            </div>
            <div className="userMsg">
              <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna
                aliqua.</span>
            </div>
            <div className="msgTime">
              <p>10:02 am • 9-07-2019</p>
            </div>
          </div> */}
          </body>
</>
  );
}

export default Scrumboard;
