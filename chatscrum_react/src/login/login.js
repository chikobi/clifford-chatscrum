import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './login.css';
import axios from 'axios';
import { Helmet } from 'react-helmet';

function Login() {
  const domainname = process.env.REACT_APP_DOMAIN_NAME;

  //Variables
  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState("");
  const [password, setPassword] = useState('');
  const [passwordError, setPasswordError] = useState("");
  const [project, setProject] = useState('');
  const [projectError, setProjectError] = useState('');
  const [message, setMessage] = useState('');
  const navigate = useNavigate();

  //Handle Changes
  const handleEmailChange = (event) => {
    setEmail(event.target.value);
    validateEmail(event.target.value);
  };

  const validateEmail = (emailValue) => {
    if (emailValue.length === 0) {
      setEmailError("Email is required");
    } else if (!/\S+@\S+\.\S+/.test(emailValue)) {
      setEmailError("Invalid email.");
    } else {
      setEmailError("");
    }
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
    validatePassword(event.target.value);
  };

  const validatePassword = (passwordValue) => {
    if (passwordValue.length === 0) {
        setPasswordError("Password is required.");
    }  else {
      setEmailError("");
    }
  };


  const handleProjectChange = (event) => {
    setProject(event.target.value);
  };

  const validateProject = (projectValue) => {
    if (projectValue.length === 0) {
        setPasswordError("Project Name is required.");
    }  else {
      setEmailError("");
    }
  };

  //Events
  /* This function is called when the user submits the login form. 
  It sends a POST request to the server with the user's email, password, and project information. 
  If the login is successful, it stores the user's information in session storage and local storage and redirects the user to the Scrumboard page. 
  If the login fails, it displays an error message. */
  const login = (event) => {
    event.preventDefault();

    const data = {
      username: email,
      password: password,
      project: project,
    };

    axios
      .post(domainname.concat("api-token-auth/"), data)
      .then((response) => {
        localStorage.setItem('full_data', JSON.stringify(response.data));
        sessionStorage.setItem('username', email);
        sessionStorage.setItem('realname', response.data.name);
        sessionStorage.setItem('role', response.data.role);
        sessionStorage.setItem('role_id', response.data.role_id);
        sessionStorage.setItem('token', response.data.token);
        sessionStorage.setItem('project_id', response.data.project_id);
        sessionStorage.setItem('to_clear_board', response.data.to_clear_board);
        sessionStorage.setItem('user_slack', response.data.user_slack);
        sessionStorage.setItem('project_slack', response.data.project_slack);
        sessionStorage.setItem('slack_username', response.data.slack_username);
        sessionStorage.setItem('ws_token', response.data.ws_token);
        sessionStorage.setItem('project_name', response.data.project_name);
        localStorage.setItem('sessiontf', response.data.to_clear_board);

        setEmail('');
        setPassword('');
        setProject('');

        setMessage('Welcome!');
        navigate(`/scrumboard/${response.data.project_id}`);
      })
      .catch((error) => {
        if (error.response && error.response.status === 400) {
          setMessage('Login Failed: Invalid Credentials.');
        } else {
          setMessage('Login Failed! Unexpected Error!');
        }
        console.error(error);
        setEmail('');
        setPassword('');
        setProject('');
      });
  };

      //Initialization of the page

        useEffect(() => {
          if (sessionStorage.getItem('token')) {
            // assuming you have a reference to the router history object
            navigate('/scrumboard/' + sessionStorage.getItem('project_id'));
          }
        }, [])

  return (
<>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"></link>
<Helmet>
  <title>Scrum | Login </title>
</Helmet>
<body>
<div className="conts">
  <nav className="mb-1 navbar navbar-expand-lg navbar-dark navc" style={{padding: "1em"}}>
    <div className="container">
      <a className="navbar-brand" href="home"><img
          src="https://res.cloudinary.com/ros4eva/image/upload/v1574765308/Rectangle_3_btfp6e.png" /></a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
        aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent-4">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item active">
            <a className="nav-link" href="login" style={{marginRight:'1em', marginTop: '5px'}}>
              <b style={{fontWeight:'650'}}> LOGIN</b>
              {/* <span className="sr-only">(current)</span> */}
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="signup">
              <button type="button" className="btn"
                style={{background: '#4DFFD4', padding: '6px 50px', fontWeight: 'bolder'}}>SIGN
                UP</button>
            </a>
          </li>
        </ul>
      </div>
    </div>

  </nav>

  <div id="gride" className="wrapper">
    <div className="row">
      <div className="col col01">
        <div className="loginForm">
          <h2 className="container" style={{fontWeight: 700}}>Log in to ChatScrum</h2>
          <div id="alertBox">

            <div id="alert-error" className="alert alert-danger alert-dismissable fade show"
              style={{marginLeft: '1em', marginBottom: '-5px', width: '95%'}}>
              <button type="button" className="close" data-dismiss="alert">&times;</button>
              {message}
            </div>
          </div>

          <form onSubmit={login} id="login" novalidate name="userLoginForm">

            <div className="formContainer">
              <div>
                <label for="email">Email</label>
                <input type="email" required placeholder="enter email..." name="email" pattern="[A-Za-z0-9._-]+@[A-Za-z0-9]+\.[a-z]{2,}$"
                  className="form-control" onChange={handleEmailChange} />
                
                {emailError && (<div className="alert alert-danger"
                  style={{border: 'none', background: 'none', padding: 'none', color: '#851111', marginTop: '-1.9em', marginBottom: '-7px', fontSize: '14px'}}>

                  <div >
                    <FontAwesomeIcon icon={faExclamationTriangle} /> {emailError}
                  </div>

                </div>)}
              </div>

              <div>
                <label for="psw">Password</label>
                <input type="password" placeholder="choose a password..." name="password" required 
                  className="form-control" minlength="3" onChange={handlePasswordChange} />
                {passwordError && (<div className="alert alert-danger"
                  style={{border: 'none', background: 'none', padding: 'none', color: '#851111', marginTop: '-1.9em', marginBottom: '-7px', fontSize: '14px'}}>

                  <div>
                    <FontAwesomeIcon icon={faExclamationTriangle} /> {passwordError}
                  </div>
                </div>)}
              </div>


              <div id="owner-Field">
                <label for="projname">Project Name</label>
                <input type="text" placeholder="enter your project name..." name="projname" onChange={handleProjectChange}
                   className="form-control" id="ownerField" required />
                {projectError && (<div className="alert alert-danger"
                  style={{border: 'none', background: 'none', padding: 'none', color: '#851111', marginTop: '-1.9em', marginBottom: '-7px', fontSize: '14px'}}>
                  <div> 
                    <FontAwesomeIcon icon={faExclamationTriangle} /> {projectError}
                    </div>
                </div>)}
              </div>
 

              <div className="lgbtn">
                <button type="submit" id="btn-one" className="btn loginbtn" 
                style={{background: '#4DFFD4', padding: '7px 60px', fontWeight: 'bolder'}}>LOGIN</button>
                <span className="psw"><a href="#" style={{fontWeight: '500'}}>Forgot password?</a></span>

              </div>
            </div>
          </form>
        </div>
      </div>
      <div className="col imgse">

      </div>
    </div>
  </div>
</div>
<div>
  <footer className=" footw">

    <div className="footer-copyright py-3">
      <div className="float-left copy" style={{color: 'rgba(0, 0, 0, 0.5)'}}>© Chatscrum 2020</div>
      <div className="float-right copy">
        <ul id="menuli">
          <li><a href="#">Support</a></li>
          <li><a href="#">Terms of use</a></li>
        </ul>
      </div>

    </div>


  </footer>
</div>
</body>
</>
  );
}

export default Login;
