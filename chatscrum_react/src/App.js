import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from "./home/home"
import Signup from './signup/signup';
import Login from './login/login';
import Support from './support/support';
import Terms from './terms/terms';
import Scrumboard from './scrumboard/scrumboard';

function App() {
    return (
    <div className='App'>
      <BrowserRouter>
        <Routes>
          <Route path="/signup" element={<Signup />} />
          <Route path="/" element={<Home />} />
          <Route path="/home" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/support" element={<Support />} />
          <Route path="/terms" element={<Terms />} />
          <Route path="/scrumboard/:project_id" element={<Scrumboard />}/> {/*This had an canActivate component in the Angular version */}
        </Routes>
      </BrowserRouter>
    </div>
    );
  }

  export default App;
  