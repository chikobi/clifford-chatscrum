import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { useState, useRef, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './signup.css';
import axios from 'axios';
import { Helmet } from 'react-helmet';

function Signup() {
  const domainname = process.env.REACT_APP_DOMAIN_NAME;

  //Variables
  const [userType, setUserType] = useState('User');
  const [message, setMessage] = useState("");
  const [projectName, setProjectName] = useState("");
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [slackAppID, setslackAppID] = useState("");
  const history = useNavigate();
  //For HTML
  //These are messages generated into the HTML code for whenever an action is made
  const [ownerFieldDisabled, setOwnerFieldDisabled] = useState(true);
  const [emailError, setEmailError] = useState("");
  const [nameError, setNameError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [alertError, setAlertError] = useState(false);
  //Refs are used to interact with the HTML code
  const sgnBtnRef = useRef(null);
  const ownerFieldRef = useRef(null);
  const ownerDashFieldRef = useRef(null);
  const alertSuccessRef = useRef(null);
  const alertErrorRef = useRef(null);
  const lodrRef = useRef(null);

  //Events

  //Updating email field based on what the user types in

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
    validateEmail(event.target.value);
  }

  const validateEmail = (emailValue) => {
  if (emailValue.length === 0) {
    setEmailError("Email is required");
  } else if (!/\S+@\S+\.\S+/.test(emailValue)) {
    setEmailError("Invalid email.");
  } else {
    setEmailError("");
  }
};

  //Updating password field errors based on what the user types in

function handlePasswordChange(event) {
  setPassword(event.target.value);
  validatePassword(event.target.value);
}

const validatePassword = (password) => {
  const passwordRegex = new RegExp("^(?=.*[!@#$%^&*?/-]).{7,16}$");
  if (password.length === 0) {
    setPasswordError("Password is required.");
  } else if (!passwordRegex.test(password)) {
    setPasswordError("Password must be combination of alphab, numbers and special characters.");
  } else {
    setPasswordError("");
  }
};

// Updating full Name field errors based on what the user types in.

function handleNameChange(event) {
  setFullName(event.target.value);
  validateFullName(event.target.value);
}

const validateFullName = (fullName) => {
  if (fullName.length === 0) {
    setNameError("Full Name is required.");
  } 
  else{
    setNameError("");
  }
};

// Handle Project Name Change

function handleProjectNameChange(event) {
  setProjectName(event.target.value);
}


  // This function enables/disbales the Project Name field depending if the AccountType
  // is User or Owner.
  function showProField(event) {
    const proField = event.target;
    const disInput = ownerFieldRef.current;
    if (proField.checked) {
      disInput.disabled = false;
      ownerDashFieldRef.current.style.opacity = '1';
      setOwnerFieldDisabled(false);
      setUserType("Owner");
    } else {
      disInput.disabled = true;
      ownerDashFieldRef.current.style.opacity = '0.4';
      setOwnerFieldDisabled(true);
      setUserType("User");
    }
  }


// This function creates a new user account by sending a POST request to the server with the provided user data.
// If the user is successfully created, the function displays a success message and redirects to the login page.
// If there is an error, the function displays an error message and clears the input fields.
// The function also sets a state variable for the Slack App ID returned by the server.
  const createUser = (event) => {
    event.preventDefault();

    axios.post(domainname.concat("api/scrumusers/"), {
      email : email,
      full_name : fullName,
      password : password,
      usertype : userType,
      projname : projectName,
    }).then(response => response.json())
    .then(data => {
      if (
        data.message == 'User Created Successfully.' ||
        data.message == 'Project Created Successfully for already existing User.'
    ) {
        alertSuccessRef.current.style.display = 'block';
        setTimeout(() => {
            history.push('/login');
        }, 3000);

        console.log(userType);
        // this.addToSlack();
    } else {
        alertErrorRef.current.style.display = 'block';
        lodrRef.current.style.display = 'none';
        setMessage('creating account.');
        setPassword('');
        setFullName('');
        setProjectName('');
    }
    setMessage(data['message']);
    setEmail('');
    setPassword('');
    setFullName('');
    setProjectName('');

    setslackAppID(data['client_id']);
})
.catch(error => {
    lodrRef.current.style.display = 'none';
    alertErrorRef.current.style.display = 'block';
    setMessage('User already exists or invalid data');
    setPassword('');
    setFullName('');
    setProjectName('');
});
};


  //Creates the loading animation for the sign up button

  function sgnBTN(){
    // Create a new span element with the "lodr" ref
    const lodrSpan = document.createElement('span');
    lodrSpan.id = 'lodr';
    lodrSpan.className = 'spinner-border spinner-border-sm mr-2';
    lodrSpan.role = 'status';
    lodrRef.current = lodrSpan; // attach the ref to the span element

    // Update the innerHTML of the button with the new span element
    sgnBtnRef.current.innerHTML = 'SIGN UP';
    sgnBtnRef.current.insertBefore(lodrSpan, sgnBtnRef.current.firstChild);

    sgnBtnRef.current.classList.add('disabled');
  }

  //Initialization of the page
    useEffect(() => {
      if (sessionStorage.getItem('token')) {
        // assuming you have a reference to the router history object
        history('/scrumboard/' + sessionStorage.getItem('project_id'));
      }
    }, []);

  return (
<>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"></link>
<Helmet>
  <title>Scrum | Signup </title>
</Helmet>
<body>
<div className="conts">
  <nav className="mb-1 navbar navbar-expand-lg navbar-dark navc" style={{padding: '1em'}}>
    <div className="container">
      <a className="navbar-brand" href="home"><img
          src="https://res.cloudinary.com/ros4eva/image/upload/v1574765308/Rectangle_3_btfp6e.png" /></a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
        aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent-4">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item active">
            <a className="nav-link" href="/login" style={{marginRight:'1em', marginTop: '5px'}}>
              <b style={{fontWeight:'650'}}> LOGIN</b>
              <span className="sr-only">(current)</span>
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/createuser">
              <button type="button" className="btn"
                style={{background: '#4DFFD4', padding: '6px 50px', fontWeight: 'bolder'}}>SIGN UP</button>
            </a>
          </li>
        </ul>
      </div>
    </div>

  </nav>

  {/* <!-- <div className="wrapper"> --> */}
  <div id="gride" className="wrapper">
    <div className="row">
      <div className="col col01">
        <div className="loginForm">
          <h2 className="container" style={{fontWeight: '700'}}>Sign up to ChatScrum</h2>
          {/* <!-- <span id="alert"></span> --> */}
          <div id="alertBox">
            <div id="alert-success" ref={alertSuccessRef} className="alert alert-success alert-dismissable fade show" style={{marginLeft: '1em'}}>
              <button type="button" className="close" data-dismiss="alert">&times;</button>
              <strong>Success!</strong> {message} redirecting to Login page...
            </div> 
            <div id="alert-error" ref={alertErrorRef} className="alert alert-danger alert-dismissable fade show"
              style={{marginLeft: '1em', marginBottom: '-5px'}}>
              <button type="button" className="close" data-dismiss="alert">&times;</button>
              <strong>Error!</strong> {message}
            </div>
          </div>


          <form onSubmit={createUser} id="signUp">

            <div className="formContainer">
              <div>
                <div>
                <label for="email">Email</label>
                <input type="email" required placeholder="enter email..." name="email" value={email} onChange={handleEmailChange}
                  className="form-control" />
                  </div>
                  <div>
                  {emailError && (
                  <div className="alert alert-danger"
                  style={{border: 'none', background: 'none', padding: 'none', color: '#851111', marginTop: '-10px', marginBottom: '-15px'}}>
                  <div>
                    <FontAwesomeIcon icon={faExclamationTriangle} /> {emailError}
                  </div>
                  </div>)}
                  </div>
              </div>



               <div>
                <div>
                <label for="psw">Password</label>
                <input type="password" placeholder="choose a password..." name="password" required value={password} onChange={handlePasswordChange}
                  className="form-control" />
                </div>
                <div>
                {passwordError && (<div className="alert alert-danger"
                  style={{border: 'none', background: 'none', padding: 'none', color: '#851111', marginTop: '-10px', marginBottom: '-15px'}}>
                    <div>
                    <FontAwesomeIcon icon={faExclamationTriangle} /> {passwordError}
                    </div>
                </div>)}
                </div>
              </div>


              <div>
                <label for="name">Full Name</label>
                <input type="text" placeholder="enter your full name..." name="fullname" value={fullName} required onChange={handleNameChange}
                  required className="form-control"
                  appForbiddenName="abc" />
                {nameError && (<div className="alert alert-danger"
                  style={{border: 'none', background: 'none', padding: 'none', color: '#851111', marginTop: '-10px', marginBottom: '-15px'}}>
                   <FontAwesomeIcon icon={faExclamationTriangle} /> {nameError}
                  </div>)}
                </div>



              <div>
                <label for="usertype">Account Type</label>
                <div className="can-toggle can-toggle--size-large">
                  <input id="c" type="checkbox" name="usertype"
                    onClick={showProField}
                    ></input>
                  <label for="c">
                    <div className="can-toggle__switch" data-unchecked="User" data-checked="Owner"></div>
                  </label>

                </div>
              </div>

              <div id="owner-Field" disabled={ownerFieldDisabled} ref={ownerDashFieldRef} style={{opacity : '0.4'}}>
                <label for="projname">Project Name</label>
                <input type="text" placeholder="enter your project name..." name="projname" value={projectName}
                 className="form-control" onChange={handleProjectNameChange} disabled={ownerFieldDisabled} id="ownerField" ref={ownerFieldRef} />

              </div>
            </div>

           
            <div className="container lgbtn">
              <button type="submit" id="btn-one" className="btn loginbtn" ref={sgnBtnRef} onClick={sgnBTN}
                style={{background: '#4DFFD4', padding: '6px 50px', fontWeight: 'bolder'}}>SIGN UP</button>
              <br></br><span style= {{fontSize: "14px"}}>By creating an account, you agree to our <a href="#"
                  style={{color: '#4DFFD4'}}>Terms of Service </a> and <a href="#" style={{color: '#4DFFD4'}}> Privacy
                  Policy</a>.</span>

            </div> 
          </form>
        </div>
      </div>
      <div className="col imgse">

      </div>
    </div>
  </div>
</div>
<div>
  <footer className=" footw">

    <div className="footer-copyright py-3">
      <div className="float-left copy" style={{color: 'rgba(0, 0, 0, 0.5)'}}>© Chatscrum 2019</div>
      <div className="float-right copy">
        <ul id="menuli">
          <li><a href="#">Support</a></li>
          <li><a href="#">Terms of use</a></li>
        </ul>
      </div>

    </div>


  </footer>
</div>
</body>
</>
  );
}

export default Signup;
